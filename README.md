# Introduction #

This is the very beginning of some small, miscellaneous Ruby applications.  The repository isn't intended for collaborative improvement.  It's the start of a collection of apps to show that I can code in Ruby.

For a couple more demonstrable Ruby on Rails projects, see my **page_maker_app** and **sample_app** repos.
