class FizzBuzz
  
  
  def isMultipleOf3?(number)
    number % 3 == 0
  end
  
  
  def isMultipleOf5?(number)
    number % 5 == 0
  end
  
  
  def isMultipleOf3and5?(number)
    isMultipleOf3?(number) && isMultipleOf5?(number)
  end

  
  def main
    (1..15).each do |x|
      
      if isMultipleOf3and5?(x)
        puts "FizzBuzz"
      elsif isMultipleOf3?(x)
        puts "Fizz"
      elsif isMultipleOf5?(x)
        puts "Buzz"
      else
        puts x
      end
      
    end
  end
  
  
end


fizzbuzz = FizzBuzz.new()
fizzbuzz.main