
def trimRecursively(str)

  return '' if str.length == 0
  
  if str[0] == ' '   # is also equivalent to str[0].eql? ' '
     
    str = str[1..str.length - 1]
    return trimRecursively(str)
    
  elsif str[str.length - 1] == ' '
    
    str = str[0..str.length - 2]
    return trimRecursively(str)
    
  else
    return str
  end

end


def displayTrimmedString(str)
  puts("x--#{str}--x")
end


displayTrimmedString(trimRecursively("    centered    "))
displayTrimmedString(trimRecursively("left justified    "))
displayTrimmedString(trimRecursively("    right justified"))
displayTrimmedString(trimRecursively("single"))
displayTrimmedString(trimRecursively(" "))
