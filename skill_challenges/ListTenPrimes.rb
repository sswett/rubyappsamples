def getNextPrime(prevPrimeNumber)
  
  primeCandidate = prevPrimeNumber

  while (true)
    primeCandidate += 1
        
    (2..primeCandidate).each do |x|   # starts at 2 because 1 is not prime
      
      if primeCandidate % x == 0
          if x == primeCandidate
              return primeCandidate
          else
              break
          end
          
      end   # if primeCandidate...
      
    end   # do

  end   # while
  
end


primeNumber = 2

(1..10).each do |x|
  puts(primeNumber)
  primeNumber = getNextPrime(primeNumber)
end
