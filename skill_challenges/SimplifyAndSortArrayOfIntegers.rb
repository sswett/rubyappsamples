require 'set'


def simplify(inputArray)
    set = inputArray.to_set   # this unique-ifies automatically
    array = set.to_a
    sorted_array = array.sort
end


unsimplifiedArray = Array[ 5, 3, 20, 4, 3, 1, 17, 5 ]
simplifiedArray = simplify(unsimplifiedArray)

print("Simplified array: ")

simplifiedArray.each do |x|
  print("#{x} ")
end
